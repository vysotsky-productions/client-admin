class Status {
  static makeTitle(status) {
    switch (status) {
      case 0:
        return "new";
      case 1:
        return "working";
      case 2:
        return "closed";
    }
    return "";
  }

  static makeColor(status) {
    switch (status) {
      case 0:
        return "green accent-4";
      case 1:
        return "orange";
      case 2:
        return "blue-grey lighten-1";
    }
  }

  static fullData() {
    return [
      {
        value: Status.NEW,
        color: Status.makeColor(this.value),
        title: Status.makeTitle(this.value)
      },
      {
        value: Status.WORKING,
        color: Status.makeColor(this.value),
        title: Status.makeTitle(this.value)
      },
      {
        value: Status.CLOSED,
        color: Status.makeColor(this.value),
        title: Status.makeTitle(this.value)
      },
    ]
  }
}

Status.NEW = 0;
Status.WORKING = 1;
Status.CLOSED = 2;

export default Status;
