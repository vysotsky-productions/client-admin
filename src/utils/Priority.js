function color(priority) {
  switch (priority) {
    case 0:
      return "red darken-4";
    case 1:
      return "orange";
    case 2:
      return "green accent-4";
  }
}

function title(priority) {
  switch (priority) {
    case 0:
      return "high";
    case 1:
      return "medium";
    case 2:
      return "low";
  }
}

class Priority {
  static makeTitle(priority) {
    return title(priority);
  }

  static makeColor(priority) {
    return color(priority);
  }

  static fullData() {
    return [
      {
        value: Priority.HIGH,
        color: color(this.value),
        title: title(this.value)
      },
      {
        value: Priority.MEDIUM,
        color: color(this.value),
        title: title(this.value)
      },
      {
        value: Priority.LOW,
        color: color(this.value),
        title: title(this.value)
      },
    ]
  }
}



Priority.HIGH = 0;
Priority.MEDIUM = 1;
Priority.LOW = 2;

export default Priority;
