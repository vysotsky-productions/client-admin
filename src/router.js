import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

const route = (path, view, meta = {}, ...children) => ({
  path,
  meta,
  name: children && children.length ? undefined : view.replace("/", ""),
  component: () =>
    import(
      /* webpackChunkName: "routes" */
      `./pages/${view}.vue`
    ),
  children
});

export const routes = [
  route("/", "Dashboard", { icon: "cloud_download", title: "Dashboard" }),
  route(
    "/inspection",
    "inspection/Index",
    { icon: "send", title: "Inspections" },
    route("", "inspection/List"),
    route("create", "inspection/Create"),
    route("show/:id", "inspection/Show")
  ),
  route(
    "/customer",
    "customer/Index",
    { icon: "delete", title: "Customers" },
    route("", "customer/List"),
    route("create", "customer/Create"),
    route("show/:id", "customer/Show")
  ),
  route(
    "/setting",
    "setting/Index",
    { icon: "warning", title: "Settings" },
    route("", "setting/List"),
    route("create", "setting/Create"),
    route("show/:id", "setting/Show")
  ),
  route(
    "/support",
    "support/Index",
    { icon: "forum", title: "Support" },
    route("", "support/List"),
    route("create", "support/Create"),
    route("show/:id", "support/Show")
  )
];

export default new Router({
  mode: "history",
  routes
});
