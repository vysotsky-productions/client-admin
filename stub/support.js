import moment from "moment";
import Priority from "./../src/utils/Priority";
import Status from "./../src/utils/Status";
import img1 from "./../src/assets/picStub1.png";
import img2 from "./../src/assets/picStub2.png";

export default [
  {
    id: "HI-249",
    date: moment([2019, 8, 5]),
    created_by: "Michael Ihavealongername",
    assigned_to: "John Supportman",
    priority: Priority.MEDIUM,
    status: Status.WORKING,
    subject: "Incorrect amount on the sales dashboard",
    messages: 4,
    description:
      "The sales dashboard doesn't display the correct amount for the sale when I update the quantity ordered. I have to log out and back in before the new amount is reflected on the dashboard.",
    attachments: [
      {
        path: "",
        item: img1
      },
      {
        path: "",
        item: img2
      }
    ],
    recently_activity: [
      {
        name: "John Supportman",
        date: moment([2019, 5, 9, 14, 37]),
        message:
          "What should the value be? Is it correct in the exported report?"
      },
      {
        name: "Michael Ihavealongername",
        date: moment([2019, 4, 9, 19, 45]),
        message:
          "I don't know when it started happening. I don't think I've ever paid attention, but now that we have been sending these new reports, I have to get this information more quickly, and I noticed the error?"
      },
      {
        name: "John Supportman",
        date: moment([2019, 4, 9, 14, 37]),
        message:
          "Were you seeing this error before the update we made last week?"
      },
      {
        name: "Michael Ihavealongername",
        date: moment([2019, 4, 9, 12, 9]),
        message:
          "The sales dashboard doesn't display the correct amount for the sale when I update the quantity ordered. I have to log out and back in before the new amount is reflected on the dashboard."
      }
    ]
  },

  {
    id: "HI-248",
    date: moment([2019, 8, 19]),
    created_by: "Sarah Sarahson",
    assigned_to: "Richard Kranium",
    priority: Priority.MEDIUM,
    status: Status.WORKING,
    subject: "Export Report option is failing.",
    messages: 2,
    description:
      "The sales dashboard doesn't display the correct amount for the sale when I update the quantity ordered. I have to log out and back in before the new amount is reflected on the dashboard.",
    attachments: [
      {
        path: "",
        item: img1
      },
      {
        path: "",
        item: img2
      }
    ],
    recently_activity: [
      {
        name: "Richard Kranium",
        date: moment([2019, 8, 9, 11, 24]),
        message:
          "Fixed a bug. Please verify that it is working properly. If so please clo..."
      },
      {
        name: "Michael Ihavealongername",
        date: moment([2019, 4, 9, 19, 45]),
        message:
          "I don't know when it started happening. I don't think I've ever paid attention, but now that we have been sending these new reports, I have to get this information more quickly, and I noticed the error?"
      }
    ]
  },

  {
    id: "HI-247",
    date: moment([2019, 8, 18]),
    created_by: "Jim Beam",
    assigned_to: "John Supportman",
    priority: Priority.MEDIUM,
    status: Status.WORKING,
    subject: "Wrong color on report title page.",
    messages: 1,
    description:
      "The sales dashboard doesn't display the correct amount for the sale when I update the quantity ordered. I have to log out and back in before the new amount is reflected on the dashboard.",
    attachments: [
      {
        path: "",
        item: img1
      },
      {
        path: "",
        item: img2
      }
    ],
    recently_activity: [
      {
        name: "John Supportman",
        date: moment([2019, 0, 1, 13, 43]),
        message:
          "You can adjust the color on the report by visiting the setting page I..."
      }
    ]
  },

  {
    id: "HI-246",
    date: moment([2019, 8, 15]),
    created_by: "Tobias Fuhke",
    assigned_to: "Bill Billingsly",
    priority: Priority.HIGH,
    status: Status.CLOSED,
    subject: "Incorrect amount on the sales dashboard",
    messages: 3,    description:
      "The sales dashboard doesn't display the correct amount for the sale when I update the quantity ordered. I have to log out and back in before the new amount is reflected on the dashboard.",
    attachments: [
      {
        path: "",
        item: img1
      },
      {
        path: "",
        item: img2
      }
    ],

    recently_activity: [
      {
        name: "John Supportman",
        date: moment([2019, 5, 9, 14, 37]),
        message: "[Closed Ticket]"
      }
    ]
  },

  {
    id: "HI-245",
    date: moment([2019, 8, 25]),
    created_by: "Bat Manning",
    assigned_to: "John Supportman",
    priority: Priority.MEDIUM,
    status: Status.WORKING,
    subject: "Incorrect amount on the sales dashboard",
    messages: 3,
    description:
      "The sales dashboard doesn't display the correct amount for the sale when I update the quantity ordered. I have to log out and back in before the new amount is reflected on the dashboard.",
    attachments: [
      {
        path: "",
        item: img1
      },
      {
        path: "",
        item: img2
      }
    ],
    recently_activity: [
      {
        name: "John Supportman",
        date: moment([2019, 5, 9, 14, 37]),
        message:
          "What should the value be? Is it correct in the exported report?"
      }
    ]
  },

  {
    id: "HI-244",
    date: moment([2019, 8, 20]),
    created_by: "Darkwing Duck",
    assigned_to: "Richard Kranium",
    priority: Priority.LOW,
    status: Status.NEW,
    subject: "My favorite color is blue.",
    messages: 0,
    description:
      "The sales dashboard doesn't display the correct amount for the sale when I update the quantity ordered. I have to log out and back in before the new amount is reflected on the dashboard.",
    attachments: [
      {
        path: "",
        item: img1
      },
      {
        path: "",
        item: img2
      }
    ],
    recently_activity: []
  }
];
